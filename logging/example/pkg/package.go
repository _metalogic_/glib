package pkg

import (
	"log/slog"
)

func TestLogLevel(level string) {
	slog.Error("pkg logging error", "level", level)
	slog.Warn("pkg logging warn", "level", level)
	slog.Info("pkg logging info", "level", level)
	slog.Debug("pkg logging debug", "level", level)
}
