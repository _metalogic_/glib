package main

import (
	"flag"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"bitbucket.org/_metalogic_/build"
	"bitbucket.org/_metalogic_/glib/logging"
	"bitbucket.org/_metalogic_/glib/logging/example/pkg"
)

var (
	info     build.BuildInfo
	levelFlg string
)

func init() {
	flag.StringVar(&levelFlg, "level", "info", "set log level to one of debug, info, warning, error")

	info = build.Info

	version := info.String()
	command := info.Name()

	flag.Usage = func() {
		fmt.Printf("Project %s:\n\n", version)
		fmt.Printf("Usage: %s -help (this message) | %s [options]:\n\n", command, command)
		flag.PrintDefaults()
	}
}

func main() {
	flag.Parse()

	logger := slog.New(slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{Level: logging.Level(levelFlg, slog.LevelInfo)}))

	fmt.Printf("set default log level to %s\n", strings.ToUpper(levelFlg))

	slog.SetDefault(logger)

	fmt.Printf("\nMain Logs\n")

	slog.Error("logging error", "level", levelFlg)
	slog.Warn("logging warn", "level", levelFlg)
	slog.Info("logging info", "level", levelFlg)
	slog.Debug("logging debug", "level", levelFlg)

	fmt.Printf("\nPackage Logs\n")

	pkg.TestLogLevel(levelFlg)
}
