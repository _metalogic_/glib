package logging

import (
	"context"
	"log/slog"
	"os"
	"strings"
)

// Level returns a log level configured by the value of levelFlg or, if
// levelFlg is empty the value of LOG_LEVEL set in environment.
// otherwise returns the default value passed in the call
func Level(levelFlg string, deflt slog.Level) slog.Level {
	level := levelFlg
	if level == "" {
		level = os.Getenv("LOG_LEVEL")
	}
	switch strings.ToUpper(level) {
	case "DEBUG":
		return slog.LevelDebug
	case "INFO":
		return slog.LevelInfo
	case "WARN":
		return slog.LevelWarn
	case "ERROR":
		return slog.LevelError
	default:
		return deflt
	}
}

// Threshold returns the level name of the lowest log level at which logs are emitted
func Threshold(logger *slog.Logger, levels ...slog.Level) string {
	var threshold slog.Level
	if len(levels) == 0 {
		levels = []slog.Level{slog.LevelError, slog.LevelWarn, slog.LevelInfo, slog.LevelDebug}
	}
	for _, level := range levels {
		if logger.Enabled(context.Background(), level) {
			threshold = level
		}
	}
	return threshold.String()
}
