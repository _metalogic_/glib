package email

import (
	"regexp"
	"strings"
)

func Validate(email string) bool {
	if len(email) < 3 || len(email) > 320 {
		return false
	}
	var emailRegex = regexp.MustCompile("^[a-zA-Z0-9]([a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]{0,63})?@[a-zA-Z0-9.-]{0,251}[a-zA-Z0-9-]\\.[a-zA-Z][a-zA-Z0-9-]{0,62}[a-zA-Z0-9]$")

	return emailRegex.MatchString(strings.ToLower(email))
}
