package password

import (
	"fmt"
	"log/slog"

	"bitbucket.org/_metalogic_/password/checker"
	"bitbucket.org/_metalogic_/password/validator"
	"golang.org/x/crypto/bcrypt"
)

// Hash - the cost parameter is a measure of how long it takes to generate the hash, values range from 4 to 31, default is 10
func Hash(password string, bcryptCost int) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcryptCost)
	return string(bytes), err
}

func Validate(password string) error {
	// entropy is a float64, representing password strength in base 2 (bits)
	const minEntropyBits = 60

	entropy := validator.GetEntropy(password)
	err := validator.Validate(password, minEntropyBits)
	if err != nil {
		return err
	}

	slog.Debug(fmt.Sprintf("password entropy is sufficient: %f; checking pwned password database", entropy))

	pwned, msg, err := checker.Pwned(password)
	if err != nil {
		return fmt.Errorf("check pwned password API failed: %s", err)
	}
	if pwned {
		slog.Warn("password has been pwned", "message", msg)
		return fmt.Errorf("password has been pwned")
	}
	slog.Debug("password not found in pwned passwords database")

	return nil
}
