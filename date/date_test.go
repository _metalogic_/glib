package date

import (
	"fmt"
	"testing"
	"time"
)

// AgeTest
type AgeTest struct {
	Name      string
	Birthdate string
	AtDate    string
	Expected  int
}

// DateTest
type DateTest struct {
	Name     string
	Input    string
	Expected string
}

// LeapTest
type LeapTest struct {
	Name     string
	Year     string
	Expected bool
}

func TestAgeAt(test *testing.T) {

	tests := []AgeTest{

		{
			Name:      "On the day",
			Birthdate: "1955-06-29",
			AtDate:    "2005-06-29",
			Expected:  50,
		},
		{
			Name:      "Before the day",
			Birthdate: "1955-06-29",
			AtDate:    "2005-06-28",
			Expected:  49,
		},
		{
			Name:      "birthdate and atdate (= birthday - 1) fall in a leap year",
			Birthdate: "2020-02-29",
			AtDate:    "2024-02-28",
			Expected:  3,
		},
		{
			Name:      "birthdate and atdate (= birthday) fall in a leap year",
			Birthdate: "2020-02-29",
			AtDate:    "2024-02-29",
			Expected:  4,
		},
		{
			Name:      "birthdate 1896-02-29 falls in a leap year; atdate 1900-02-28 does NOT",
			Birthdate: "1896-02-29",
			AtDate:    "1900-02-28",
			Expected:  3,
		},
		{
			Name:      "birthdate 1896-02-29 falls in a leap year; atdate 1900-03-01 does NOT",
			Birthdate: "1896-02-29",
			AtDate:    "1900-03-01",
			Expected:  4,
		},
		{
			Name:      "birthdate 2020-02-29 falls in a leap year; atdate 2025-03-01 does NOT",
			Birthdate: "2020-02-29",
			AtDate:    "2025-03-01",
			Expected:  5,
		},
		{
			Name:      "atdate 2024-02-29 falls in a leap year; birthdate 1999-02-28 does NOT",
			Birthdate: "2019-02-28",
			AtDate:    "2024-02-29",
			Expected:  5,
		},
		{
			Name:      "atdate 2024-02-29 falls in a leap year; birthdate 1900-02-28 does NOT",
			Birthdate: "1900-02-28",
			AtDate:    "2024-02-29",
			Expected:  124,
		},
		{
			Name:      "atdate 2024-02-29 falls in a leap year; birthdate 1900-03-01 does NOT",
			Birthdate: "1900-03-01",
			AtDate:    "2024-02-29",
			Expected:  123,
		},
	}
	runAgeAtTests(tests, test)
}

func TestLast(test *testing.T) {

	tests := []DateTest{

		{
			Name:     "Last in current year",
			Input:    "Jan-31",
			Expected: "2022-01-31",
		},
		{
			Name:     "Last in previous year",
			Input:    "Jun-29",
			Expected: "2021-06-29",
		},
	}
	runLastTests(tests, test)
}

func TestNext(test *testing.T) {

	tests := []DateTest{

		{
			Name:     "Next",
			Input:    "Jun-29",
			Expected: "2022-06-29",
		},
		{
			Name:     "Single MINUS",
			Input:    "Jan-31",
			Expected: "2023-01-31",
		},
	}
	runNextTests(tests, test)
}

func TestIsLeap(test *testing.T) {

	tests := []LeapTest{

		{
			Name:     "1900",
			Year:     "1900",
			Expected: false,
		},
		{
			Name:     "2020",
			Year:     "2020",
			Expected: true,
		},
		{
			Name:     "2200",
			Year:     "2200",
			Expected: false,
		},
		{
			Name:     "2400",
			Year:     "2400",
			Expected: true,
		},
	}
	runLeapTests(tests, test)
}

func runAgeAtTests(tests []AgeTest, test *testing.T) {

	fmt.Printf("Running %d AgeAt test cases...\n", len(tests))

	// Run the test cases.
	for _, t := range tests {

		birthdate, err := time.Parse("2006-01-02", t.Birthdate)
		if err != nil {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		atdate, err := time.Parse("2006-01-02", t.AtDate)
		if err != nil {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		result := AgeAt(birthdate, atdate)

		if result != t.Expected {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, t.Expected)
			test.Fail()
		}
	}
}

func runLastTests(tests []DateTest, test *testing.T) {
	var err error

	fmt.Printf("Running %d Last test cases...\n", len(tests))

	// Run the test cases.
	for _, t := range tests {

		if err != nil {

			test.Logf("Test '%s' failed to parse: '%s'", t.Name, err)
			test.Fail()
			continue
		}

		next, err := Last(t.Input)

		if err != nil {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		result := next.Format("2006-01-02")
		if result != t.Expected {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, t.Expected)
			test.Fail()
		}
	}
}

func runNextTests(tests []DateTest, test *testing.T) {
	var err error

	fmt.Printf("Running %d Next test cases...\n", len(tests))

	// Run the test cases.
	for _, t := range tests {

		if err != nil {

			test.Logf("Test '%s' failed to parse: '%s'", t.Name, err)
			test.Fail()
			continue
		}

		next, err := Next(t.Input)

		if err != nil {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		result := next.Format("2006-01-02")
		if result != t.Expected {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, t.Expected)
			test.Fail()
		}
	}
}

func runLeapTests(tests []LeapTest, test *testing.T) {

	fmt.Printf("Running %d Leap test cases...\n", len(tests))

	// Run the test cases.
	for _, t := range tests {

		y, err := time.Parse("2006", t.Year)

		if err != nil {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Encountered error: %s", err.Error())
			test.Fail()
			continue
		}

		result := IsLeap(y)

		if result != t.Expected {

			test.Logf("Test '%s' failed", t.Name)
			test.Logf("Evaluation result '%v' does not match expected: '%v'", result, t.Expected)
			test.Fail()
		}
	}
}
