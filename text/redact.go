package text

func Redact(secret string) string {
	l := len(secret)
	if l == 0 {
		return "*EMPTY*"
	}
	if l <= 10 {
		return "*REDACTED*"
	}
	return secret[:4] + " *REDACTED* " + secret[l-4:]
}
